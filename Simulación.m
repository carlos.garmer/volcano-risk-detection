%%Verlet 2
g = 9.81;
h = input("Ingrese el valor del paso del h >>>"); %Modificable
kp = input("Ingrese el valor de la resistencia del aire >>>"); %Modificable
hvolcan = input("Ingrese el valor de la altura del volcan >>>");

disp("Proyectil Azul")
m = input("Ingrese el valor de la masa del proyectil >>>"); %Modificable
v = input("Ingrese el valor de la velocidad del proyectil  >>>"); %Modificable
angle = input("Ingrese el valor del angulo del proyectil  >>>"); %Modificable


disp("Proyectil Rojo")
m1 = input("Ingrese el valor de la masa del proyectil >>>"); %Modificable
v1 = input("Ingrese el valor de la velocidad del proyectil  >>>"); %Modificable
angle1 = input("Ingrese el valor del angulo del proyectil  >>>"); %Modificable

disp("Proyectil Amarillo")
m2 = input("Ingrese el valor de la masa del proyectil >>>"); %Modificable
v2 = input("Ingrese el valor de la velocidad del proyectil  >>>"); %Modificable
angle2 = input("Ingrese el valor del angulo del proyectil  >>>"); %Modificable


% Inicializa vector de tiempo.
t = 0:h:100;

% Inicializa los vectores de posición, velocidad y aceleración
%Y0
sy = zeros(size(t));
vy = zeros(size(t));
ay = zeros(size(t));
vmy = zeros(size(t));

%Y1
sy1 = zeros(size(t));
vy1 = zeros(size(t));
ay1 = zeros(size(t));
vmy1 = zeros(size(t));

%Y2
sy2 = zeros(size(t));
vy2 = zeros(size(t));
ay2 = zeros(size(t));
vmy2 = zeros(size(t));

%X
sx = zeros(size(t));
vx = zeros(size(t));
ax = zeros(size(t));
vmx = zeros(size(t));

%X1
sx1 = zeros(size(t));
vx1 = zeros(size(t));
ax1 = zeros(size(t));
vmx1 = zeros(size(t));

%X2
sx2 = zeros(size(t));
vx2 = zeros(size(t));
ax2 = zeros(size(t));
vmx2 = zeros(size(t));

% Asignar los valores iniciales y
sy(1) = hvolcan; %Modificable
vy(1) = v*sind(angle);
ay(1) = -g - (kp/m)*vy(1);
vmy(1) = vy(1);

sy1(1) = hvolcan; %Modificable
vy1(1) = v1*sind(angle1);
ay1(1) = -g - (kp/m1)*vy1(1);
vmy1(1) = vy1(1);

sy2(1) = hvolcan; %Modificable
vy2(1) = v2*sind(angle2);
ay2(1) = -g - (kp/m1)*vy2(1);
vmy2(1) = vy2(1);

% Asignar los valores iniciales x
sx(1) = 0;
vx(1) = v*cosd(angle);
ax(1) =(kp/m)*v(1)^2;
vmx(1) = vx(1);

sx1(1) = 0;
vx1(1) = v1*cosd(angle1);
ax1(1) =(kp/m1)*v1(1)^2;
vmx1(1) = vx1(1);

sx2(1) = 0;
vx2(1) = v2*cosd(angle2);
ax2(1) =(kp/m2)*v2(1)^2;
vmx2(1) = vx2(1);

% Calcula con Euler los valroes de s, v, y a
k = 2;
while(sy(k-1)>=0)
    % Velocidad intermedia    
    vmy(k) = vy(k-1) + 0.5*ay(k-1)*h;
    vmx(k) = vx(k-1) + 0.5*ax(k-1)*h;

    % Nueva aceleración
    ay(k) = -g - (kp/m)*vmy(k);
    ax(k) = (-kp/m)*vmx(k);

    % Nueva velocidad
    vy(k) = vmy(k) + 0.5*ay(k)*h;
    vx(k) = vmx(k) + 0.5*ax(k)*h;

    % Nueva posición
    sy(k) = sy(k-1) + h*vmy(k-1) + 0.5*ay(k-1)*h^2;
    sx(k) = sx(k-1) + h*vmx(k-1) + 0.5*ax(k-1)*h^2;

    k = k + 1;
end

for i=1:length(t)
    if sy(i) <= 0
        sy(i) = sy(k-1);
        sx(i) = sx(k-1);
        t(i) = t(k-1);
    end 
end







% Calcula con Euler los valroes de s, v, y a
k = 2;
while(sy1(k-1)>=0)
    % Velocidad intermedia    
    vmy1(k) = vy1(k-1) + 0.5*ay1(k-1)*h;
    vmx1(k) = vx1(k-1) + 0.5*ax1(k-1)*h;

    % Nueva aceleración
    ay1(k) = -g - (kp/m1)*vmy1(k);
    ax1(k) = (-kp/m1)*vmx1(k);

    % Nueva velocidad
    vy1(k) = vmy1(k) + 0.5*ay1(k)*h;
    vx1(k) = vmx1(k) + 0.5*ax1(k)*h;

    % Nueva posición
    sy1(k) = sy1(k-1) + h*vmy1(k-1) + 0.5*ay1(k-1)*h^2;
    sx1(k) = sx1(k-1) + h*vmx1(k-1) + 0.5*ax1(k-1)*h^2;

    k = k + 1;
end

for i=1:length(t)
    if sy1(i) <= 0
        sy1(i) = sy1(k-1);
        sx1(i) = sx1(k-1);
        t(i) = t(k-1);
    end 
end




% Calcula con Euler los valroes de s, v, y a
k = 2;
while(sy2(k-1)>=0)
    % Velocidad intermedia    
    vmy2(k) = vy2(k-1) + 0.5*ay2(k-1)*h;
    vmx2(k) = vx2(k-1) + 0.5*ax2(k-1)*h;

    % Nueva aceleración
    ay2(k) = -g - (kp/m2)*vmy2(k);
    ax2(k) = (-kp/m2)*vmx2(k);

    % Nueva velocidad
    vy2(k) = vmy2(k) + 0.5*ay2(k)*h;
    vx2(k) = vmx2(k) + 0.5*ax2(k)*h;

    % Nueva posición
    sy2(k) = sy2(k-1) + h*vmy2(k-1) + 0.5*ay2(k-1)*h^2;
    sx2(k) = sx2(k-1) + h*vmx2(k-1) + 0.5*ax2(k-1)*h^2;

    k = k + 1;
end

for i=1:length(t)
    if sy2(i) <= 0
        sy2(i) = sy2(k-1);
        sx2(i) = sx2(k-1);
        t(i) = t(k-1);
    end 
end


hold on
title('Modelación de Proyectil Balístico')
xlabel('Desplazamiento en X (m)')
ylabel('Desplazamiento en Y (m)')
f = msgbox('Operation Completed');
set(f, 'position', [300 300 100 300]);

azul = false;
rojo = false;
amarillo = false;
for k = 1:length(sx)
    v = sqrt(vx(k)^2 + vy(k)^2);
    plot(sx(k), sy(k), 'c.')
    
    v1 = sqrt(vx1(k)^2 + vy1(k)^2);
    plot(sx1(k), sy1(k), 'r.')
    
    v2 = sqrt(vx2(k)^2 + vy2(k)^2);
    plot(sx2(k), sy2(k), 'y.')
    
    set(findobj(f,'Tag','MessageBox'),'String',({"PROYECTIL AZUL:"; "   V= "+v;"   Vx= "+vx(k);"   Vy= "+vy(k);"   X= "+num2str(sx(k));"   Y= "+num2str(sy(k));
        " ";"PROYECTIL ROJO:"; "   V= "+v1;"   Vx= "+vx1(k);"   Vy= "+vy1(k);"   X= "+num2str(sx1(k));"   Y= "+num2str(sy1(k));
        " ";"PROYECTIL AMARILLO:"; "   V= "+v2;"   Vx= "+vx2(k);"   Vy= "+vy2(k);"   X= "+num2str(sx2(k));"   Y= "+num2str(sy2(k));}));

    if(mod(k,100)==0)
        text(sx(k), sy(k), "v = " + num2str(v))
        text(sx1(k), sy1(k), "v = " + num2str(v1))
        text(sx2(k), sy2(k), "v = " + num2str(v2))
    end
    
    maxX = max([sx(:);sx1(:);sx2(:)]);
    maxY = max([sy(:);sy1(:);sy2(:)]);
    
    xlim([0 maxX])
    ylim([0 maxY])
    drawnow
    
    
    if(sy(k+1)==sy(k) && azul == false)
        vxf = vx(k);
        vyf = vy(k);
        azul = true
    end
    if(sy1(k+1)==sy1(k) && rojo == false)
        vxf1 = vx1(k);
        vyf1 = vy1(k);
        rojo = true
    end 
    if(sy2(k+1)==sy2(k) && amarillo == false)
        vxf2 = vx2(k);
        vyf2 = vy2(k);
        amarillo = true
    end  
    if(sy(k+1)==sy(k) && sy1(k+1)==sy1(k) && sy2(k+1)==sy2(k))
        break
    end  
end
g = msgbox('Operation Completed');
set(g, 'position', [850 300 200 350]); %tamaño en y es la ultima
set(findobj(g,'Tag','MessageBox'),'String',({"PROYECTIL AZUL:"; "   Alt. max= "+max(sy);"   Desp. X= "+max(sx);"   Vfx=   "+vxf;"   Vfy=   "+vyf;"   Ang.=   "+angle; "   K=   "+kp;"   m= "+m;
    "PROYECTIL ROJO:"; "   Alt. max= "+max(sy1);"   Desp. X= "+max(sx1);"   Vfx=   "+vxf1;"   Vfy=   "+vyf1;"   Ang.=   "+angle1; "   K=   "+kp;"   m= "+m1;
    "PROYECTIL AMARILLO:"; "   Alt. max= "+max(sy2);"   Desp. X= "+max(sx2);"   Vfx=   "+vxf2;"   Vfy=   "+vyf2;"   Ang.=   "+angle2; "   K=   "+kp;"   m= "+m2;}));


disp("finish")
hold off


